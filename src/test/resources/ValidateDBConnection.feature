Feature: Employees that report to payam
  @st
  Scenario: Validating the employees that report to payam
    Given User is able to connect to database
    When User inputs the correct quary
    Then User should see the data displayed as below

      |Jason|Mallin|
      |Michael|Rogers|
      |Ki|Gee|
      |Hazel|Philtanker|
      |Kelly|Chung|
      |Jennifer|Dilly|
      |Timothy|Gates|
      |Randall|Perkins|